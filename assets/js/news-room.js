let activeNr = 0;

const nrs = document.querySelectorAll("#nr");

nrs[activeNr].classList.remove("d-none");

function nextNr() {
  if (activeNr < nrs.length - 1) {
    nrs[activeNr].classList.add("d-none");
    activeNr = activeNr + 1;
    nrs[activeNr].classList.remove("d-none");
  } else {
    activeNr = 0;
    nrs[activeNr].classList.add("d-none");
    nrs[0].classList.remove("d-none");
  }
}

function prevNr() {
  if (activeNr > 0) {
    nrs[activeNr].classList.add("d-none");
    activeNr = activeNr - 1;
    nrs[activeNr].classList.remove("d-none");
  } else {
    activeNr = nrs.length - 1;
    nrs[activeNr].classList.remove("d-none");
    nrs[0].classList.add("d-none");
  }
}
