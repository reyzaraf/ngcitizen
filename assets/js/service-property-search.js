let activeStdLux = "std";
let activeCEA = "carribean";

const stdLux = document.getElementById("std-lux");
const standard = stdLux.children[0];
const luxury = stdLux.children[1];

const stdSec = document.querySelector("#std-lux-section #std-section");
const luxSec = document.querySelector("#std-lux-section #lux-section");

const stdCarribean = document.querySelector(
  "#std-lux-section #std-section #std-carribean"
);
const stdEurope = document.querySelector(
  "#std-lux-section #std-section #std-europe"
);
const stdAsian = document.querySelector(
  "#std-lux-section #std-section #std-asian"
);

const luxCarribean = document.querySelector(
  "#std-lux-section #lux-section #lux-carribean"
);
const luxEurope = document.querySelector(
  "#std-lux-section #lux-section #lux-europe"
);
const luxAsian = document.querySelector(
  "#std-lux-section #lux-section #lux-asian"
);

const carribeanEuropeAsian = document.getElementById("carribean-europe-asian");

standard.addEventListener("click", (e) => {
  activeStdLux = "std";
  e.target.classList.add("active");
  stdSec.classList.remove("d-none");
  luxury.classList.remove("active");
  luxSec.classList.add("d-none");
  disableCEA();
  disableCEAContent();
  carribeanEuropeAsian.children[0].classList.add("active");
  stdCarribean.classList.remove("d-none");
});

luxury.addEventListener("click", (e) => {
  activeStdLux = "lux";
  e.target.classList.add("active");
  luxSec.classList.remove("d-none");
  standard.classList.remove("active");
  stdSec.classList.add("d-none");
  disableCEA();
  disableCEAContent();
  carribeanEuropeAsian.children[0].classList.add("active");
  luxCarribean.classList.remove("d-none");
});

const disableCEA = () => {
  for (let i = 0; i < carribeanEuropeAsian.children.length; i++) {
    const element = carribeanEuropeAsian.children[i];
    if (element.classList.contains("active")) {
      element.classList.remove("active");
    }
  }
};

const disableCEAContent = () => {
  if (!stdCarribean.classList.contains("d-none")) {
    stdCarribean.classList.add("d-none");
  }
  if (!stdEurope.classList.contains("d-none")) {
    stdEurope.classList.add("d-none");
  }
  if (!stdAsian.classList.contains("d-none")) {
    stdAsian.classList.add("d-none");
  }
  //
  if (!luxCarribean.classList.contains("d-none")) {
    luxCarribean.classList.add("d-none");
  }
  if (!luxEurope.classList.contains("d-none")) {
    luxEurope.classList.add("d-none");
  }
  if (!luxAsian.classList.contains("d-none")) {
    luxAsian.classList.add("d-none");
  }
};

for (let i = 0; i < carribeanEuropeAsian.children.length; i++) {
  const element = carribeanEuropeAsian.children[i];
  element.addEventListener("click", (e) => {
    disableCEA();
    e.target.classList.add("active");
    disableCEAContent();
    if (activeStdLux == "std") {
      // for standard
      //   console.log(e.target.innerText);
      switch (e.target.innerText) {
        case "Carribean":
          stdCarribean.classList.remove("d-none");
          break;

        case "Europe":
          stdEurope.classList.remove("d-none");
          break;

        case "Asian":
          stdAsian.classList.remove("d-none");
          break;

        default:
          alert("error");
          break;
      }
    } else {
      // for luxury
      //   console.log(e.target.innerText, " lux");
      switch (e.target.innerText) {
        case "Carribean":
          luxCarribean.classList.remove("d-none");
          break;

        case "Europe":
          luxEurope.classList.remove("d-none");
          break;

        case "Asian":
          luxAsian.classList.remove("d-none");
          break;

        default:
          alert("error");
          break;
      }
    }
  });
}

// sliders

let activeCarribeanStd = 0;

const carribeanStd = document.querySelectorAll("#carribean-items-std");

// carribeanStd[activeCarribeanStd].classList.replace("d-none", "d-flex");

function nextCarribeanStd() {
  if (activeCarribeanStd < carribeanStd.length - 1) {
    carribeanStd[activeCarribeanStd].classList.replace("d-flex", "d-none");
    activeCarribeanStd = activeCarribeanStd + 1;
    carribeanStd[activeCarribeanStd].classList.replace("d-none", "d-flex");
  } else {
    activeCarribeanStd = 0;
    carribeanStd[activeCarribeanStd].classList.replace("d-flex", "d-none");
    carribeanStd[0].classList.replace("d-none", "d-flex");
  }
}

function prevCarribeanStd() {
  if (activeCarribeanStd > 0) {
    carribeanStd[activeCarribeanStd].classList.replace("d-flex", "d-none");
    activeCarribeanStd = activeCarribeanStd - 1;
    carribeanStd[activeCarribeanStd].classList.replace("d-none", "d-flex");
  } else {
    activeCarribeanStd = carribeanStd.length - 1;
    carribeanStd[activeCarribeanStd].classList.replace("d-none", "d-flex");
    carribeanStd[0].classList.replace("d-flex", "d-none");
  }
}

let activeEuropeStd = 0;

const europeStd = document.querySelectorAll("#europe-items-std");

// europeStd[activeEuropeStd].classList.replace("d-none", "d-flex");

function nextEuropeStd() {
  if (activeEuropeStd < europeStd.length - 1) {
    europeStd[activeEuropeStd].classList.replace("d-flex", "d-none");
    activeEuropeStd = activeEuropeStd + 1;
    europeStd[activeEuropeStd].classList.replace("d-none", "d-flex");
  } else {
    activeEuropeStd = 0;
    europeStd[activeEuropeStd].classList.replace("d-flex", "d-none");
    europeStd[0].classList.replace("d-none", "d-flex");
  }
}

function prevEuropeStd() {
  if (activeEuropeStd > 0) {
    europeStd[activeEuropeStd].classList.replace("d-flex", "d-none");
    activeEuropeStd = activeEuropeStd - 1;
    europeStd[activeEuropeStd].classList.replace("d-none", "d-flex");
  } else {
    activeEuropeStd = europeStd.length - 1;
    europeStd[activeEuropeStd].classList.replace("d-none", "d-flex");
    europeStd[0].classList.replace("d-flex", "d-none");
  }
}

let activeAsianStd = 0;

const asianStd = document.querySelectorAll("#asian-items-std");

// asianStd[activeAsianStd].classList.replace("d-none", "d-flex");

function nextAsianStd() {
  if (activeAsianStd < asianStd.length - 1) {
    asianStd[activeAsianStd].classList.replace("d-flex", "d-none");
    activeAsianStd = activeAsianStd + 1;
    asianStd[activeAsianStd].classList.replace("d-none", "d-flex");
  } else {
    activeAsianStd = 0;
    asianStd[activeAsianStd].classList.replace("d-flex", "d-none");
    asianStd[0].classList.replace("d-none", "d-flex");
  }
}

function prevAsianStd() {
  if (activeAsianStd > 0) {
    asianStd[activeAsianStd].classList.replace("d-flex", "d-none");
    activeAsianStd = activeAsianStd - 1;
    asianStd[activeAsianStd].classList.replace("d-none", "d-flex");
  } else {
    activeAsianStd = asianStd.length - 1;
    asianStd[activeAsianStd].classList.replace("d-none", "d-flex");
    asianStd[0].classList.replace("d-flex", "d-none");
  }
}

let activeCarribeanLux = 0;

const carribeanLux = document.querySelectorAll("#carribean-items-lux");

// carribeanLux[activeCarribeanLux].classList.replace("d-none", "d-flex");

function nextCarribeanLux() {
  if (activeCarribeanLux < carribeanLux.length - 1) {
    carribeanLux[activeCarribeanLux].classList.replace("d-flex", "d-none");
    activeCarribeanLux = activeCarribeanLux + 1;
    carribeanLux[activeCarribeanLux].classList.replace("d-none", "d-flex");
  } else {
    activeCarribeanLux = 0;
    carribeanLux[activeCarribeanLux].classList.replace("d-flex", "d-none");
    carribeanLux[0].classList.replace("d-none", "d-flex");
  }
}

function prevCarribeanLux() {
  if (activeCarribeanLux > 0) {
    carribeanLux[activeCarribeanLux].classList.replace("d-flex", "d-none");
    activeCarribeanLux = activeCarribeanLux - 1;
    carribeanLux[activeCarribeanLux].classList.replace("d-none", "d-flex");
  } else {
    activeCarribeanLux = carribeanLux.length - 1;
    carribeanLux[activeCarribeanLux].classList.replace("d-none", "d-flex");
    carribeanLux[0].classList.replace("d-flex", "d-none");
  }
}

let activeEuropeLux = 0;

const europeLux = document.querySelectorAll("#europe-items-lux");

// europeLux[activeEuropeLux].classList.replace("d-none", "d-flex");

function nextEuropeLux() {
  if (activeEuropeLux < europeLux.length - 1) {
    europeLux[activeEuropeLux].classList.replace("d-flex", "d-none");
    activeEuropeLux = activeEuropeLux + 1;
    europeLux[activeEuropeLux].classList.replace("d-none", "d-flex");
  } else {
    activeEuropeLux = 0;
    europeLux[activeEuropeLux].classList.replace("d-flex", "d-none");
    europeLux[0].classList.replace("d-none", "d-flex");
  }
}

function prevEuropeLux() {
  if (activeEuropeLux > 0) {
    europeLux[activeEuropeLux].classList.replace("d-flex", "d-none");
    activeEuropeLux = activeEuropeLux - 1;
    europeLux[activeEuropeLux].classList.replace("d-none", "d-flex");
  } else {
    activeEuropeLux = europeLux.length - 1;
    europeLux[activeEuropeLux].classList.replace("d-none", "d-flex");
    europeLux[0].classList.replace("d-flex", "d-none");
  }
}

let activeAsianLux = 0;

const asianLux = document.querySelectorAll("#asian-items-lux");

// asianLux[activeAsianLux].classList.replace("d-none", "d-flex");

function nextAsianLux() {
  if (activeAsianLux < asianLux.length - 1) {
    asianLux[activeAsianLux].classList.replace("d-flex", "d-none");
    activeAsianLux = activeAsianLux + 1;
    asianLux[activeAsianLux].classList.replace("d-none", "d-flex");
  } else {
    activeAsianLux = 0;
    asianLux[activeAsianLux].classList.replace("d-flex", "d-none");
    asianLux[0].classList.replace("d-none", "d-flex");
  }
}

function prevAsianLux() {
  if (activeAsianLux > 0) {
    asianLux[activeAsianLux].classList.replace("d-flex", "d-none");
    activeAsianLux = activeAsianLux - 1;
    asianLux[activeAsianLux].classList.replace("d-none", "d-flex");
  } else {
    activeAsianLux = asianLux.length - 1;
    asianLux[activeAsianLux].classList.replace("d-none", "d-flex");
    asianLux[0].classList.replace("d-flex", "d-none");
  }
}