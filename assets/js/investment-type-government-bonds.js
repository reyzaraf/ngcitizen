let activeGb = 0;

const gbs = document.querySelectorAll("#gbs");

gbs[activeGb].classList.replace("d-none", "d-flex");

function nextGbs() {
  if (activeGb < gbs.length - 1) {
    gbs[activeGb].classList.replace("d-flex", "d-none");
    activeGb = activeGb + 1;
    gbs[activeGb].classList.replace("d-none", "d-flex");
  } else {
    activeGb = 0;
    gbs[activeGb].classList.replace("d-flex", "d-none");
    gbs[0].classList.replace("d-none", "d-flex");
  }
}

function prevGbs() {
  if (activeGb > 0) {
    gbs[activeGb].classList.replace("d-flex", "d-none");
    activeGb = activeGb - 1;
    gbs[activeGb].classList.replace("d-none", "d-flex");
  } else {
    activeGb = gbs.length - 1;
    gbs[activeGb].classList.replace("d-none", "d-flex");
    gbs[0].classList.replace("d-flex", "d-none");
  }
}
