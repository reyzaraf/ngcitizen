let activeIos = 0;

const ios = document.querySelectorAll("#ios");

// ios[activeIos].classList.replace("d-none", "d-flex");

function nextIos() {
  if (activeIos < ios.length - 1) {
    ios[activeIos].classList.replace("d-flex", "d-none");
    activeIos = activeIos + 1;
    ios[activeIos].classList.replace("d-none", "d-flex");
  } else {
    activeIos = 0;
    ios[ios.length-1].classList.replace("d-flex", "d-none");
    ios[0].classList.replace("d-none", "d-flex");
  }
}

function prevIos() {
  if (activeIos > 0) {
    ios[activeIos].classList.replace("d-flex", "d-none");
    activeIos = activeIos - 1;
    ios[activeIos].classList.replace("d-none", "d-flex");
  } else {
    activeIos = ios.length - 1;
    ios[activeIos].classList.replace("d-none", "d-flex");
    ios[0].classList.replace("d-flex", "d-none");
  }
}
