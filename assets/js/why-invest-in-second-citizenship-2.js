let activeWiisc2 = 0;

const wiisc2 = document.querySelectorAll("#wiisc2");

wiisc2[activeWiisc2].classList.replace("d-none", "d-flex");

function nextWiisc2() {
  if (activeWiisc2 < wiisc2.length - 1) {
    wiisc2[activeWiisc2].classList.replace("d-flex", "d-none");
    activeWiisc2 = activeWiisc2 + 1;
    wiisc2[activeWiisc2].classList.replace("d-none", "d-flex");
  }else {
    activeWiisc2 = 0;
    wiisc2[activeWiisc2].classList.replace("d-flex", "d-none");
    wiisc2[0].classList.replace("d-none", "d-flex");
  }
}

function prevWiisc2() {
  if (activeWiisc2 > 0) {
    wiisc2[activeWiisc2].classList.replace("d-flex", "d-none");
    activeWiisc2 = activeWiisc2 - 1;
    wiisc2[activeWiisc2].classList.replace("d-none", "d-flex");
  } else {
    activeWiisc2 = wiisc2.length - 1;
    wiisc2[activeWiisc2].classList.replace("d-none", "d-flex");
    wiisc2[0].classList.replace("d-flex", "d-none");
  }
}
