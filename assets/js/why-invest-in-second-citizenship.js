let activeWiisc = 0;

const wiisc = document.querySelectorAll("#wiisc");

wiisc[activeWiisc].classList.replace("d-none", "d-flex");

function nextWiisc() {
  if (activeWiisc < wiisc.length - 1) {
    wiisc[activeWiisc].classList.replace("d-flex", "d-none");
    activeWiisc = activeWiisc + 1;
    wiisc[activeWiisc].classList.replace("d-none", "d-flex");
  } else {
    activeWiisc = 0;
    wiisc[activeWiisc].classList.replace("d-flex", "d-none");
    wiisc[0].classList.replace("d-none", "d-flex");
  }
}

function prevWiisc() {
  if (activeWiisc > 0) {
    wiisc[activeWiisc].classList.replace("d-flex", "d-none");
    activeWiisc = activeWiisc - 1;
    wiisc[activeWiisc].classList.replace("d-none", "d-flex");
  } else {
    activeWiisc = wiisc.length - 1;
    wiisc[activeWiisc].classList.replace("d-none", "d-flex");
    wiisc[0].classList.replace("d-flex", "d-none");
  }
}
