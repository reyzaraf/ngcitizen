// scroll
addEventListener("scroll", () => {
  const offset = window.scrollY;
  const topNavbar = document.getElementById("top-nav");
  const navbar = document.getElementById("navbar");
  const brand = document.getElementById("brand");
  const navlinks = document.querySelectorAll(".nav-link");
  const navbarToggle = document.getElementById("custom-navbar-toggle");
  const sidebarContact = document.getElementById("sidebar-contact");
  const langMobile = document.getElementById("lang-mobile");
  const checkEligibility = document.getElementById("checkEligibility");

  if (offset >= 150) {
    topNavbar.classList.add("top-nav-active");
    navbar.classList.add("bg-white", "shadow");
    navbarToggle.classList.remove("text-white");
    for (let index = 0; index < navlinks.length; index++) {
      navlinks[index].style = "color: #1F2027 !important";
    }
    brand.classList.replace("brand-light", "brand-dark");
    brand.classList.replace("brand-md-light", "brand-md-dark");
    sidebarContact.style = null;
    langMobile.classList.remove("text-white");
    checkEligibility.style.backgroundColor = "#323848";
  } else {
    topNavbar.classList.remove("top-nav-active");
    navbar.classList.remove("bg-white", "shadow");
    navbarToggle.classList.add("text-white");
    for (let index = 0; index < navlinks.length; index++) {
      navlinks[index].style = null;
    }
    if (!navbar.classList.contains("bg-nav-white")) {
      brand.classList.replace("brand-dark", "brand-light");
    }
    brand.classList.replace("brand-md-dark", "brand-md-light");
    sidebarContact.style = "display: none !important";
    if (navbarToggleButton.classList.contains("collapsed")) {
      langMobile.classList.add("text-white");
    }
    checkEligibility.style.backgroundColor = null;
  }
});

const topNavbar = document.getElementById("top-nav");
const navbar = document.getElementById("navbar");
const navbarToggleButton = document.getElementById("navbar-toggle-button");
const navbarToggle = document.getElementById("custom-navbar-toggle");
const brand = document.getElementById("brand");
const langMobile = document.getElementById("lang-mobile");
// const dropdownTop = document.querySelectorAll("#dropdown-top");

// toggle click
navbarToggleButton.addEventListener("click", () => {
  if (!navbarToggleButton.classList.contains("collapsed")) {
    // console.log("button closed");
    navbarToggle.innerText = "menu";
    navbar.classList.remove("bg-nav-white");
    navbarToggle.style = null;
    topNavbar.classList.remove("top-nav-active-2");
    if (!navbar.classList.contains("bg-white")) {
      brand.classList.replace("brand-dark", "brand-light");
      langMobile.classList.add("text-white");
    }
    // dropdownTop.forEach((data) => {
    //   data.children[0].children[0].classList.remove("arrow-rotate-90");
    // });
  } else {
    navbarToggle.innerText = "close";
    navbar.classList.add("bg-nav-white");
    navbarToggle.style = "color: #1F2027 !important";
    brand.classList.replace("brand-light", "brand-dark");
    langMobile.classList.remove("text-white");
    topNavbar.classList.add("top-nav-active-2");
    // console.log("button open");
  }
});

// rotate arrow navbar
// const dropdownTop = document.querySelectorAll("#dropdown-top");
// for (let index = 0; index < dropdownTop.length; index++) {
//   dropdownTop[index].addEventListener(
//     "click",
//     () => {
//       dropdownTop.forEach((data, i) => {
//         if (i == index) {
//           // console.log('x');
//         } else {
//           data.children[0].children[0].classList.remove("arrow-rotate-90");
//         }
//       });
//       dropdownTop[index].children[0].children[0].classList.toggle(
//         "arrow-rotate-90"
//       );
//     },
//     true
//   );
// }
