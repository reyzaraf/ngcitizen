let activePbs = 0;

const pbs = document.querySelectorAll("#pbs");

// pbs[activePbs].classList.replace("d-none", "d-flex");

function nextPbs() {
  if (activePbs < pbs.length - 1) {
    pbs[activePbs].classList.replace("d-flex", "d-none");
    activePbs = activePbs + 1;
    pbs[activePbs].classList.replace("d-none", "d-flex");
  } else {
    activePbs = 0;
    pbs[pbs.length-1].classList.replace("d-flex", "d-none");
    pbs[0].classList.replace("d-none", "d-flex");
  }
}

function prevPbs() {
  if (activePbs > 0) {
    pbs[activePbs].classList.replace("d-flex", "d-none");
    activePbs = activePbs - 1;
    pbs[activePbs].classList.replace("d-none", "d-flex");
  } else {
    activePbs = pbs.length - 1;
    pbs[activePbs].classList.replace("d-none", "d-flex");
    pbs[0].classList.replace("d-flex", "d-none");
  }
}
