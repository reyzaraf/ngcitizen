// eu slider
let activeRbi = 0;

const rbis = document.querySelectorAll("#rbis");

rbis[activeRbi].classList.replace("d-none", "d-flex");

function nextRbis() {
  if (activeRbi < rbis.length - 1) {
    rbis[activeRbi].classList.replace("d-flex", "d-none");
    activeRbi = activeRbi + 1;
    rbis[activeRbi].classList.replace("d-none", "d-flex");
  } else {
    activeRbi = 0;
    rbis[activeRbi].classList.replace("d-flex", "d-none");
    rbis[0].classList.replace("d-none", "d-flex");
  }
}

function prevRbis() {
  if (activeRbi > 0) {
    rbis[activeRbi].classList.replace("d-flex", "d-none");
    activeRbi = activeRbi - 1;
    rbis[activeRbi].classList.replace("d-none", "d-flex");
  } else {
    activeRbi = rbis.length - 1;
    rbis[activeRbi].classList.replace("d-none", "d-flex");
    rbis[0].classList.replace("d-flex", "d-none");
  }
}

// asian slider
let activeRbi2 = 0;

const rbis2 = document.querySelectorAll("#rbis2");

rbis2[activeRbi2].classList.replace("d-none", "d-flex");

function nextRbis2() {
  if (activeRbi2 < rbis2.length - 1) {
    rbis2[activeRbi2].classList.replace("d-flex", "d-none");
    activeRbi2 = activeRbi2 + 1;
    rbis2[activeRbi2].classList.replace("d-none", "d-flex");
  } else {
    activeRbi2 = 0;
    rbis2[activeRbi2].classList.replace("d-flex", "d-none");
    rbis2[0].classList.replace("d-none", "d-flex");
  }
}

function prevRbis2() {
  if (activeRbi2 > 0) {
    rbis2[activeRbi2].classList.replace("d-flex", "d-none");
    activeRbi2 = activeRbi2 - 1;
    rbis2[activeRbi2].classList.replace("d-none", "d-flex");
  } else {
    activeRbi2 = rbis2.length - 1;
    rbis2[activeRbi2].classList.replace("d-none", "d-flex");
    rbis2[0].classList.replace("d-flex", "d-none");
  }
}

// eu / asia
const euAsia = document.getElementById("eu-or-asian");
const eu = euAsia.children[0];
const asia = euAsia.children[1];

// detail
const euAsiaDetail = document.getElementById("eu-or-asian-detail");
const euDetail = euAsiaDetail.children[0];
const asiaDetail = euAsiaDetail.children[1];

eu.addEventListener("click", () => {
  eu.classList.add("active");
  euDetail.style = null;
  asia.classList.remove("active");
  asiaDetail.style = "display: none !important";
});
asia.addEventListener("click", () => {
  eu.classList.remove("active");
  euDetail.style = "display: none !important";
  asia.classList.add("active");
  asiaDetail.style = null;
});
