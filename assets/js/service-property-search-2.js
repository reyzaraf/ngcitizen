let activeStdLux = "std";
let activeSub = "dominica-citizenship";

const stdLux = document.getElementById("std-lux");
const stdLuxOptions = document.getElementById("stdLuxOptions");

const std = document.getElementById("std");
const lux = document.getElementById("lux");

const resetStdLux = () => {
  for (const data in stdLux.children) {
    if (Object.hasOwnProperty.call(stdLux.children, data)) {
      const element = stdLux.children[data];
      element.classList.remove("active");
    }
  }
};

const resetSub = () => {
  let x = document.querySelectorAll("[data-sub]");
  // console.log(x);
  x.forEach((data) => {
    // data.classList.add("d-none");
    for (const y in data.children) {
      if (Object.hasOwnProperty.call(data.children, y)) {
        const element = data.children[y];
        // console.log(element);
        element.classList.add("d-none");
      }
    }
  });
};

// standard or luxury
for (const data in stdLux.children) {
  if (Object.hasOwnProperty.call(stdLux.children, data)) {
    const element = stdLux.children[data];
    element.addEventListener("click", (e) => {
      let dataFire = e.target.dataset.fire;

      resetStdLux();

      e.target.classList.add("active");

      if (dataFire == "standard") {
        activeStdLux = "std";
        lux.classList.add("d-none");
        std.classList.remove("d-none");
      } else if (dataFire == "luxury") {
        activeStdLux = "lux";
        lux.classList.remove("d-none");
        std.classList.add("d-none");
      }
    });
  }
}

// select options
stdLuxOptions.addEventListener("change", (e) => {
  let selectedValue = stdLuxOptions.options[stdLuxOptions.selectedIndex].value;
  // console.log(selectedValue);
  let sub = document.querySelectorAll(`#${selectedValue}`);
  // console.log(sub);
  resetSub();
  sub.forEach((data) => {
    data.classList.remove("d-none");
  });
});

// sliders
let activeStdDominicaCitizenship = 0;

const stdDc = document.querySelectorAll("#stdDc");

function nextstdDc() {
  if (activeStdDominicaCitizenship < stdDc.length - 1) {
    stdDc[activeStdDominicaCitizenship].classList.replace("d-flex", "d-none");
    activeStdDominicaCitizenship = activeStdDominicaCitizenship + 1;
    stdDc[activeStdDominicaCitizenship].classList.replace("d-none", "d-flex");
  } else {
    activeStdDominicaCitizenship = 0;
    stdDc[activeStdDominicaCitizenship].classList.replace("d-flex", "d-none");
    stdDc[0].classList.replace("d-none", "d-flex");
  }
}

function prevstdDc() {
  if (activeStdDominicaCitizenship > 0) {
    stdDc[activeStdDominicaCitizenship].classList.replace("d-flex", "d-none");
    activeStdDominicaCitizenship = activeStdDominicaCitizenship - 1;
    stdDc[activeStdDominicaCitizenship].classList.replace("d-none", "d-flex");
  } else {
    activeStdDominicaCitizenship = stdDc.length - 1;
    stdDc[activeStdDominicaCitizenship].classList.replace("d-none", "d-flex");
    stdDc[0].classList.replace("d-flex", "d-none");
  }
}

let activeStdDominicaCitizenship2 = 0;

const stdDc2 = document.querySelectorAll("#stdDc2");

function nextstdDc2() {
  if (activeStdDominicaCitizenship2 < stdDc2.length - 1) {
    stdDc2[activeStdDominicaCitizenship2].classList.replace("d-flex", "d-none");
    activeStdDominicaCitizenship2 = activeStdDominicaCitizenship2 + 1;
    stdDc2[activeStdDominicaCitizenship2].classList.replace("d-none", "d-flex");
  } else {
    activeStdDominicaCitizenship2 = 0;
    stdDc2[activeStdDominicaCitizenship2].classList.replace("d-flex", "d-none");
    stdDc2[0].classList.replace("d-none", "d-flex");
  }
}

function prevstdDc2() {
  if (activeStdDominicaCitizenship2 > 0) {
    stdDc2[activeStdDominicaCitizenship2].classList.replace("d-flex", "d-none");
    activeStdDominicaCitizenship2 = activeStdDominicaCitizenship2 - 1;
    stdDc2[activeStdDominicaCitizenship2].classList.replace("d-none", "d-flex");
  } else {
    activeStdDominicaCitizenship2 = stdDc2.length - 1;
    stdDc2[activeStdDominicaCitizenship2].classList.replace("d-none", "d-flex");
    stdDc2[0].classList.replace("d-flex", "d-none");
  }
}

let activeLuxDominicaCitizenship = 0;

const luxDc = document.querySelectorAll("#luxDc");

function nextluxDc() {
  if (activeLuxDominicaCitizenship < luxDc.length - 1) {
    luxDc[activeLuxDominicaCitizenship].classList.replace("d-flex", "d-none");
    activeLuxDominicaCitizenship = activeLuxDominicaCitizenship + 1;
    luxDc[activeLuxDominicaCitizenship].classList.replace("d-none", "d-flex");
  } else {
    activeLuxDominicaCitizenship = 0;
    luxDc[activeLuxDominicaCitizenship].classList.replace("d-flex", "d-none");
    luxDc[0].classList.replace("d-none", "d-flex");
  }
}

function prevluxDc() {
  if (activeLuxDominicaCitizenship > 0) {
    luxDc[activeLuxDominicaCitizenship].classList.replace("d-flex", "d-none");
    activeLuxDominicaCitizenship = activeLuxDominicaCitizenship - 1;
    luxDc[activeLuxDominicaCitizenship].classList.replace("d-none", "d-flex");
  } else {
    activeLuxDominicaCitizenship = luxDc.length - 1;
    luxDc[activeLuxDominicaCitizenship].classList.replace("d-none", "d-flex");
    luxDc[0].classList.replace("d-flex", "d-none");
  }
}

let activeLuxDominicaCitizenship2 = 0;

const luxDc2 = document.querySelectorAll("#luxDc2");

function nextluxDc2() {
  if (activeLuxDominicaCitizenship2 < luxDc2.length - 1) {
    luxDc2[activeLuxDominicaCitizenship2].classList.replace("d-flex", "d-none");
    activeLuxDominicaCitizenship2 = activeLuxDominicaCitizenship2 + 1;
    luxDc2[activeLuxDominicaCitizenship2].classList.replace("d-none", "d-flex");
  } else {
    activeLuxDominicaCitizenship2 = 0;
    luxDc2[activeLuxDominicaCitizenship2].classList.replace("d-flex", "d-none");
    luxDc2[0].classList.replace("d-none", "d-flex");
  }
}

function prevluxDc2() {
  if (activeLuxDominicaCitizenship2 > 0) {
    luxDc2[activeLuxDominicaCitizenship2].classList.replace("d-flex", "d-none");
    activeLuxDominicaCitizenship2 = activeLuxDominicaCitizenship2 - 1;
    luxDc2[activeLuxDominicaCitizenship2].classList.replace("d-none", "d-flex");
  } else {
    activeLuxDominicaCitizenship2 = luxDc2.length - 1;
    luxDc2[activeLuxDominicaCitizenship2].classList.replace("d-none", "d-flex");
    luxDc2[0].classList.replace("d-flex", "d-none");
  }
}
