let activePbds = 0;

const pbds = document.querySelectorAll("#pbds");

pbds[activePbds].classList.replace("d-none", "d-flex");

function nextPbds() {
  if (activePbds < pbds.length - 1) {
    pbds[activePbds].classList.replace("d-flex", "d-none");
    activePbds = activePbds + 1;
    pbds[activePbds].classList.replace("d-none", "d-flex");
  } else {
    activePbds = 0;
    pbds[activePbds].classList.replace("d-flex", "d-none");
    pbds[0].classList.replace("d-none", "d-flex");
  }
}

function prevPbds() {
  if (activePbds > 0) {
    pbds[activePbds].classList.replace("d-flex", "d-none");
    activePbds = activePbds - 1;
    pbds[activePbds].classList.replace("d-none", "d-flex");
  } else {
    activePbds = pbds.length - 1;
    pbds[activePbds].classList.replace("d-none", "d-flex");
    pbds[0].classList.replace("d-flex", "d-none");
  }
}
