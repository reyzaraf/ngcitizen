const ourProgramFixedAddonNavbar = document.getElementById(
  "our-program-fixed-addon-navbar"
);

const ourProgramFixedAddonNavbarButton =
  ourProgramFixedAddonNavbar.children[0].children[1];
const ourProgramFixedAddonNavbarList = document.getElementById('our-program-fixed-addon-navbar-list')

ourProgramFixedAddonNavbarButton.addEventListener("click", (e) => {
  if (ourProgramFixedAddonNavbarList.classList.contains("d-none")) {
    ourProgramFixedAddonNavbarList.classList.remove("d-none");
    ourProgramFixedAddonNavbarButton.innerHTML = "close";
  } else {
    ourProgramFixedAddonNavbarList.classList.add("d-none");
    ourProgramFixedAddonNavbarButton.innerHTML = "menu";
  }
});
