let activeCbis = 0;

const cbis = document.querySelectorAll("#cbis");

cbis[activeCbis].classList.replace("d-none", "d-flex");

function nextCbis() {
  if (activeCbis < cbis.length - 1) {
    cbis[activeCbis].classList.replace("d-flex", "d-none");
    activeCbis = activeCbis + 1;
    cbis[activeCbis].classList.replace("d-none", "d-flex");
  } else {
    activeCbis = 0;
    cbis[activeCbis].classList.replace("d-flex", "d-none");
    cbis[0].classList.replace("d-none", "d-flex");
  }
}

function prevCbis() {
  if (activeCbis > 0) {
    cbis[activeCbis].classList.replace("d-flex", "d-none");
    activeCbis = activeCbis - 1;
    cbis[activeCbis].classList.replace("d-none", "d-flex");
  } else {
    activeCbis = cbis.length - 1;
    cbis[activeCbis].classList.replace("d-none", "d-flex");
    cbis[0].classList.replace("d-flex", "d-none");
  }
}
