let activeWcds = 0;

const wcds = document.querySelectorAll("#wcds");

// wcds[activeWcds].classList.replace("d-none", "d-flex");

function nextWcds() {
  if (activeWcds < wcds.length - 1) {
    wcds[activeWcds].classList.replace("d-flex", "d-none");
    activeWcds = activeWcds + 1;
    wcds[activeWcds].classList.replace("d-none", "d-flex");
  } else {
    activeWcds = 0;
    wcds[wcds.length-1].classList.replace("d-flex", "d-none");
    wcds[0].classList.replace("d-none", "d-flex");
  }
}

function prevWcds() {
  if (activeWcds > 0) {
    wcds[activeWcds].classList.replace("d-flex", "d-none");
    activeWcds = activeWcds - 1;
    wcds[activeWcds].classList.replace("d-none", "d-flex");
  } else {
    activeWcds = wcds.length - 1;
    wcds[activeWcds].classList.replace("d-none", "d-flex");
    wcds[0].classList.replace("d-flex", "d-none");
  }
}
